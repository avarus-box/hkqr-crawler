"use strict";

import url from "url";
import http from 'http';


const env = process.env;

const proxy = {
    protocol: "http:",
    hostname: "127.0.0.1",
    port: 8888,
};

const proxyRequests = function () {
    var proxyUrl = url.format(proxy);
    env.http_proxy = proxyUrl;
    env.https_proxy = proxyUrl;
    env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
};

const unproxyRequests = function () {
    env.http_proxy = "";
    env.https_proxy = "";
    env.NODE_TLS_REJECT_UNAUTHORIZED = "";
};

export default {
    proxyRequests,
    unproxyRequests
};