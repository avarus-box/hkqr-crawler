import rp from 'request-promise';
import cheerio from 'cheerio';
import _ from 'lodash';
import mysql from 'mysql2/promise';
import {
    winston
} from './config';
import {
    sleep
} from './helper';
import fiddler from './fiddler';

// Debug
// fiddler.proxyRequests();

const crawlerConfig = {
    totalPage: 4,

    isUseDB: true,
    mysql: {
        host: 'localhost',
        user: 'hkqr',
        password: '1234',
        database: 'hkqr',
    }
};


// Default options
rp.defaults({
    jar: rp.jar()
});

class crawler {
    constructor() {
        this.isSave = true;
        this.connection = null;

        this.cookieJar = rp.jar();
        this.rp = rp.defaults({
            jar: this.cookieJar
        });
    }

    async saveItem(item) {
        // TODO:: Check exisrinf
        winston.debug(`Saving item ${item.id}:`, item);

        let query;
        let rows;
        let fields;
        winston.error(this.connection);
        // Check existance
        [rows, fields] = await this.connection.execute('SELECT * FROM `course` WHERE `id` = ?', [item.id]);

        if (rows.length > 0) {
            // Update or Ignore
        } else {
            // Insert
            await this.connection.query('INSERT INTO course SET ?', item);
        }
        
    }

    async parseItem(html) {
        const data = {};
        const $ = cheerio.load(html);


        $(`.detailsContainer`).each(function (index, el) {
            //winston.error(el);
            if ($(this).find('.textBold') !== null && $(this).find('.col40') !== null) {
                if ($(this).find('.textBold').length > 0 && $(this).find('.col40').length > 0) {

                    const title = $(this).find('.textBold').first().text().trim();
                    const desc = $(this).find('.col40').first().text().trim();
                    if (title.startsWith('Title of Qualification:')) {
                        data.qualification = desc;
                    } else if (title.startsWith('Title of Qualification (Chinese):')) {
                        data.qualificationCht = desc;
                    } else if (title.startsWith('Title of Learning Programme:')) {
                        data.learning = desc;
                    } else if (title.startsWith('Title of Learning Programme (Chinese):')) {
                        data.learningCht = desc;
                    } else if (title.startsWith('QR Registration No.')) {
                        data.regNo = desc;
                    } else if(title.startsWith('Registration Validity Period')){
                        data.RegistrationPeriod = desc;
                    }else if(title.startsWith('Registration Status')){
                        data.RegistrationStatus = desc;
                    }else if(title.startsWith('Level')){
                        data.Level = desc;
                    }else if(title.startsWith('Credit')){
                        data.Level = desc;
                    }else if(title.startsWith('Credit Accumulation and Transfer')){
                        data.CreditAccumulationTransfer = desc;
                    }else if(title.startsWith('CAT Info')){
                        data.CATInfo = desc;
                    }else if(title.startsWith('Primary Area of Study and Training')){
                        data.PrimaryAreaStudy = desc;
                    }else if(title.startsWith('Sub-area (Primary Area of Study and Training)')){
                        data.SubAreaPrimaryAreaStudy = desc;
                    }else if(title.startsWith('Other Area of Study and Training')){
                        data.OtherAreaStudy = desc;
                    }else if(title.startsWith('Sub-area (Other Area of Study and Training)')){
                        data.SubAreaOtherAreaStudy = desc;
                    }else if(title.startsWith('Industry and Branch')){
                        data.IndustryBranch = desc;
                    }else if(title.startsWith('Name of Operator / Agency')){
                        data.NameOperatorAgency = desc;
                    }else if(title.startsWith('Name of Operator / Agency(Chinese)')){
                        data.NameOperatorAgencyChinese = desc;
                    }else if(title.startsWith('Mode of Delivery')){
                        data.ModeOfDelivery = desc;
                    }else if(title.startsWith('SCS-based Programme')){
                        data.SCSbasedProgramme = desc;
                    }else if(title.startsWith('SGC-based Programme')){
                        data.SGCbasedProgramme = desc;
                    }else if(title.startsWith('Non-Local Programme')){
                        data.NonLocalProgramme = desc;
                    }else if(title.startsWith('Granting Body')){
                        data.GrantingBody = desc;
                    }else if(title.startsWith('Granting Body (Chinese)')){
                        data.GrantingBodyChinese = desc;
                    }else if(title.startsWith('Country/Region of Operator/Agency')){
                        data.CountryOfOperatorAgency = desc;
                    }else if(title.startsWith('Country/Region of Granting Body')){
                        data.CountryOfGrantingBody = desc;
                    }else if(title.startsWith('Programme / Qualification Info.')){
                        data.ProgrammeQualificationInfo = desc;
                    }else if(title.startsWith('Prog. / Qualification Enq. Phone No.')){
                        data.ProgQualificationPhoneNo = desc;
                    }else if(title.startsWith('Operator/Agency Reference Code')){
                        data.OperatorAgencyRefCode = desc;
                    }
                    // winston.info(title, desc);
                }
            }
        });

        return data;
    }

    async parseListing(html) {
        let ids = [];
        const $ = cheerio.load(html);

        // winston.info($(`.searchResultTr`).length);
        $(`.searchResultTr input[name='listCheckBox']`).each(function () {
            ids.push($(this).val());
        });
        return ids;
    }


    async processItem(id) {
        winston.debug(`Processing item ${id}...`);

        // http://www.hkqr.gov.hk/HKQR/enquirePublishedQr.do?qr_reg_no=17/000513/L5&provider_code=357&show_prl_ind=N&show_related_names=N&timestamp=1501481771103

        // Prepare request options
        const providerCode = 357; // ???
        const timestamp = (new Date()).getTime();
        const options = {
            jar: this.cookieJar,
            method: 'GET',
            uri: `http://www.hkqr.gov.hk/HKQR/enquirePublishedQr.do?qr_reg_no=${id}&provider_code=${providerCode}&show_prl_ind=N&show_related_names=N&timestamp=${timestamp}`,
        };

        winston.debug(`Requesting item ${id}...`);
        const html = await this.rp(options);
        const data = await this.parseItem(html);
        winston.debug(`item ${id}:`, data);

        if (!_.isEmpty(data) && this.isSave) {
            const item = {
                ...{
                    id
                },
                ...data
            };
            this.saveItem(item);
        }
    }


    async processListing(page) {
        winston.info(`Processing page ${page}...`);

        // Prepare request options
        const options = {
            jar: this.cookieJar,
            method: 'POST',
            uri: 'http://www.hkqr.gov.hk/HKQR/listQualification.do',
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            form: {
                action_type: `Search`,
                orderBy: `credit`,
                order: `desc`,
                recordPerPage: `20`,
                industry_id: ``,
                branch_id: ``,
                qualifications_title: ``,
                country_code: ``,
                delivery_mode_code: ``,
                area_study_code_1: ``,
                sub_area_study_code_1: ``,
                types_of_qualification: ``,
                types_of_qua_option_code: ``,
                types_of_qua_option_desc: ``,
                provider_code: ``,
                qr_country_code: ``,
                qr_reg_no_shortlist: ``,
                pageFlag: `Search`,
                advance_search: ``,
                qualifications_status: `'1current','2prospective'`,
                qr_level: ``,
                show_related_names: ``,
                cat_status_ind: ``,
                granting_body: ``,
                exclude_industry_list: ``,
                keywords: `E.g. Title, Area of Study, Agency, Industry / branch`,
                types_of_qualifications: `E.g. RPLs, SCS-based, NLQs`,
                qr_level_desc: `I.e. L1 - L7`,
                qr_reg_no: `QR Registration No of the Qualifications`,
                credit: ``,
                toCredit: ``,
                cat_status_ind_desc1: `Select from the following`,
                area_study_desc: `Select from the following`,
                sub_area_desc: `Select from the following`,
                industry_id_desc: `Select from the following`,
                branch_id_desc: `Select from the following`,
                lp_title: `Partial / Full Title of Learning Programme`,
                qualifications_title_desc: `E.g. Bachelor, Higher Diploma`,
                granting_body_desc: `Direct input or choose from the pull-down list`,
                qr_country_desc: `E.g. HK, UK, Aust`,
                provider_desc: `Direct input or choose from the pull-down list`,
                country_desc: `E.g. HK`,
                delivery_mode_desc: `E.g. FT, PT, E-Learning`,
                lp_code: `Reference Code of the Qualification`,
                qualifications_status_desc: `Current,Prospective`,
                totalOperator: ``,
                totalGrantingBody: ``,
                totalRecords: `0`,
                tarPage: page,
                totalPages: `0`,
            },
        };

        winston.debug(`Requesting page ${page}...`);
        const html = await this.rp(options);

        // Get Record IDs
        const recordIds = await this.parseListing(html);
        winston.debug(`Found ${recordIds.length} items at page ${page}: `, recordIds);

        // Process Items
        for (let j = 0; j < recordIds.length; j += 1) {
            await this.processItem(recordIds[j]);
        }

        winston.debug(`page ${page} length: ${html.length}`);

        // Sleep
        await sleep(50);
    }

    // async getCookie() {
    //     try {
    //         const options = {
    //             method: 'GET',
    //             uri: 'http://www.hkqr.gov.hk/HKQRPRD/web/hkqr-en/search/qr-search/',
    //         };
    //         await this.rp(options);

    //         await this.rp({
    //             ...options,
    //             ...{
    //                 uri: 'http://www.hkqr.gov.hk/HKQR/changeLangDisp.do?languageMode=en&timestamp=1500011829429',
    //             }
    //         });

    //         return true;
    //     } catch (error) {
    //         winston.error('Cannot Get Cookie. Error:', error);
    //         return false;
    //     }
    // }

    async start() {
        winston.info('Crawler Started');

        winston.info('Checking Database');
        if (crawlerConfig.isUseDB) {
            try {
                this.connection = await mysql.createConnection(crawlerConfig.mysql);
                winston.info('MySQL connected.');
            } catch (error) {
                winston.error('Cannot connect MySQL. Error:', error);
                return;
            }
        } else {
            winston.warn('Database is not using');
        }
        // winston.info('Getting Cookie');
        // const result = await this.getCookie();
        // if (!result) {
        //     return;
        // }

        for (let i = 1; i <= crawlerConfig.totalPage; i++) {
            try {
                await this.processListing(i);
            } catch (error) {
                winston.error(`Process page ${i} Failed, Error:`, error);
            }
        }

        winston.info('Crawler Completed');
    }
}


export default crawler;