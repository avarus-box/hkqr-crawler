import winston from 'winston';

// winston.remove(winston.transports.Console);
// winston.add(winston.transports.Console, {
//   //timestamp: true,
//   level: process.env.LOG_LEVEL || "info",
//   colorize: true
// });
// winston.level = process.env.LOG_LEVEL ;

// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }

const logLevel = 'debug';

const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            timestamp: tsFormat,
            level: logLevel,
            colorize: true,
            prettyPrint: function (object) {
                return JSON.stringify(object, null, 2);
            }
        }),
        //new (winston.transports.File)({ filename: 'somefile.log' })
    ],
    exitOnError: false // don't crush no error
});


export default logger;